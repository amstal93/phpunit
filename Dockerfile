FROM ubuntu:latest

ARG USERNAME=testenv
ARG PASSWORD=testenv


LABEL maintainer="Tennigkeit<tennigkeit@codereaders.eu>"
LABEL description="Custom PHPUnit"
LABEL version="1.1.0"

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get -y upgrade
# requirements
RUN apt-get -y install \
         curl apache2 \
         libapache2-mod-php php php-cli php-fpm php-json php-common php-pdo-mysql \
         php-zip php-gd php-mbstring php-curl php-xml php-pear php-bcmath php-pecl-http php-memcache \
         composer

# /requirements
RUN apt-get -y clean && apt-get -y autoclean && apt-get -y autoremove && rm -rf /var/lib/apt/lists/*

# create a user to run composer as
RUN adduser --gecos "" --disabled-password ${USERNAME}
RUN chpasswd << "${USERNAME}:${PASSWORD}"

# prepare report
RUN mkdir -p /report

# let's get started
RUN echo "${USERNAME}" >/opt/user
COPY entrypoint.sh /opt/entrypoint.sh
WORKDIR /opt/
ENTRYPOINT [ "/opt/entrypoint.sh" ]
CMD [ "${USERNAME}" ]
