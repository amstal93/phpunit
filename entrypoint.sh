#!/bin/bash

USER=$(cat /opt/user)
if [ "${1}" == "" ]
then
  AUTOLOADER=src/vendor/autoload.php
else
  AUTOLOADER="${1}"
fi

# copy and validate
cp -r /opt/app /opt/wd
chown -R "${USER}:${USER}" /opt/wd/
cd /opt/wd/src/ || exit 1

# composer install
su - "${USER}" -c "cd /opt/wd/src && composer update" &> /dev/null
RESULT=$?

if [ "${RESULT}" == "0" ]
then
  # run tests
  cd /opt/wd/ || exit 1
  su - "${USER}" -c "cd /opt/wd/ && ./src/vendor/bin/phpunit --bootstrap ${AUTOLOADER} tests" >/report/unittests.txt
  RESULT=$?

  # raw output > std out
  cat /report/unittests.txt

  # set permissions
  chmod 666 /report/unittests.txt

  # publish exit code
  exit ${RESULT}
else
  echo "Composer install failed"
  exit 1
fi