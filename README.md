# PHPUnit

A customized automation for PHPUnit

## Prerequisites

Your project structure has to look like this:

```bash
.
..
src/
src/composer.json
src/composer.lock
tests/
tests/YOUR_UNIT_TESTS
reports/
```

## Usage

### Full pattern

```bash
docker run \
    -v $PATH_TO_YOUR_PROJECT/:/opt/app:ro \
    -v $PATH_TO_YOUR_REPORTS/:/report:rw \
    registry.gitlab.com/saarphil/phpunit:latest $PATH_TO_YOUR_CLASSLOADER
```

### Minimal pattern

```bash
docker run \
    -v $PATH_TO_YOUR_PROJECT/:/opt/app:ro \
    registry.gitlab.com/saarphil/phpunit:latest
```

### Example

```bash
docker run \
    -v ${PWD}/:/opt/app:ro \
    -v ${PWD}/reports:/report:rw \
    registry.gitlab.com/saarphil/phpunit:latest src/vendor/autoloder.php
```

## Info

Reports are being printed on std out.
By default a composer update is being run and the autoloader is expected to placed in `src/vendor/autoloader.php`.
You can overwrite the autoloader by adding the path to it as an argument.
